<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Tests\TestCase;

class ProductTest extends TestCase
{
    public function testGetView()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_add_unit_to_product()
    {
        DB::table('products')
            ->update(['unit' => 100, 'status' => 1]);

        $filePath = '/tmp/file.csv';
        file_put_contents($filePath, "id,command,parameter\n1,Agregar,5");

        $this->postJson('/', [
            'csv_file' => new UploadedFile($filePath, 'test.csv', null, null, null, true),
        ])->assertStatus(200);

        $this->assertDatabaseHas('products', [
            'id' => 1,
            'unit' => 105
        ]);

        $this->assertDatabaseHas('products', [
            'id' => 2,
            'unit' => 105
        ]);
    }

    public function test_rest_unit_to_product()
    {
        DB::table('products')
            ->update(['unit' => 100, 'status' => 1]);

        $filePath = '/tmp/file.csv';
        file_put_contents($filePath, "id,command,parameter\n1,Restar,5");

        $this->postJson('/', [
            'csv_file' => new UploadedFile($filePath, 'test.csv', null, null, null, true),
        ])->assertStatus(200);

        $this->assertDatabaseHas('products', [
            'id' => 1,
            'unit' => 95
        ]);

        $this->assertDatabaseHas('products', [
            'id' => 2,
            'unit' => 95
        ]);
    }


    public function test_disable_product()
    {
        DB::table('products')
            ->update(['unit' => 100, 'status' => 1]);

        $filePath = '/tmp/file.csv';
        file_put_contents($filePath, "id,command,parameter\n1,Desactivar");

        $this->postJson('/', [
            'csv_file' => new UploadedFile($filePath, 'test.csv', null, null, null, true),
        ])->assertStatus(200);

        $this->assertDatabaseHas('products', [
            'id' => 1,
            'status' => 0
        ]);

        $this->assertDatabaseHas('products', [
            'id' => 2,
            'status' => 0
        ]);
    }

    public function test_enable_product()
    {
        DB::table('products')
            ->update(['unit' => 100, 'status' => 0]);

        $filePath = '/tmp/file.csv';
        file_put_contents($filePath, "id,command,parameter\n1,Activar");

        $this->postJson('/', [
            'csv_file' => new UploadedFile($filePath, 'test.csv', null, null, null, true),
        ])->assertStatus(200);

        $this->assertDatabaseHas('products', [
            'id' => 1,
            'status' => 1
        ]);

        $this->assertDatabaseHas('products', [
            'id' => 2,
            'status' => 1
        ]);
    }
}
