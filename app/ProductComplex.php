<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductComplex extends Model
{
    public function get_relation()
    {
        return $this->belongsTo(Product::class, 'product_required_id');
    }
}
