<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const STATUS_DISABLE = 0;
    const STATUS_AVAILABLE = 1;

    const STRING_ENABLE = "Activar";
    const STRING_DISABLE = "Desactivar";
    const STRING_ADD = "Agregar";
    const STRING_REST = "Restar";

    public function get_product_complexes()
    {
        return $this->hasMany(ProductComplex::class, "product_id");
    }
}
