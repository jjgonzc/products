<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('product/index', array('products' => $products));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('csv_file')) {
            $path = $request->file('csv_file')->getRealPath();
            $data = Excel::load($path)->get();

            if ($data->count()) {
                foreach ($data as $key => $value) {
                    $product = Product::find($value->id);
                    if ($product != null) {
                        $productComplex = $product->get_product_complexes;
                        $this->update_data($value, $product);

                        foreach ($productComplex as $product) {
                            $this->update_data($value, $product->get_relation);
                        }
                    }
                }
            }
        }

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function update_data($value, $product)
    {
        $command = $value->command;

        if ($command == Product::STRING_ENABLE || $command == Product::STRING_DISABLE) {
            $product->status = $command == Product::STRING_ENABLE ? 1 : 0;
        } elseif ($command == Product::STRING_ADD || $command == Product::STRING_REST) {
            $valueParameter = $value->parameter;
            $unit = $product->unit;
            $product->unit = $command == Product::STRING_ADD ? $unit + $valueParameter :
                $unit - $valueParameter;
        }

        $product->save();
    }
}
