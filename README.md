# Products in Laravel

Servicio creado para prueba de Merqueo

laravel 5.7

## instalación

- Necesitas instalar los modulos ext-gd y ext-zip 
- Renombrar .env.example a .env y configurar la base de datos

```sh
1- composer du
2- php artisan migrate
3- php artisan db:seed
4- php artisan serve
``` 


## Como usar

Levantar el servidor visitar

http://localhost:8000/

- En esta vista tendras toda la informacion de los productos agregados inventarios, stock, estado, entre otro

- Cuenta con un formulario para subir un archivo .csv la cual debe estar formatiado de la siguiente forma

#### example:
```csv
id,command,parameter
1,Agregar,5
2,Restar,5
3,Activar
3,Agregar,15
4,Desactivar
```

Al hacer submit el controlador va a actualizar la lista de producto dependiendo del caso ya sea (Agregar, Restar, Desactivar, Activar), Si lo producto estan asociado con otro producto porque se considera un producto compuestos todos los cambio realizado en el producto principal tambien seran reflejado en los productos compuestos todos los comando funcionan para producto relacionados

##### example.csv
Archivo agregado para hacer pruebas con los seeder y datos ya existente