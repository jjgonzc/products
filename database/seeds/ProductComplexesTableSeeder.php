<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductComplexesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_complexes')->insert([
            'id' => 1,
            'product_id' => 1,
            'product_required_id' => 2,
        ]);
        DB::table('product_complexes')->insert([
            'id' => 2,
            'product_id' => 1,
            'product_required_id' => 3,
        ]);
        DB::table('product_complexes')->insert([
            'id' => 3,
            'product_id' => 1,
            'product_required_id' => 4,
        ]);
    }
}
