<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'id' => 1,
            'name' => 'Samba',
            'reference' => 'Samba-02',
            'price' => 1000,
            'cost' => 1200,
            'unit' => 50,
            'status' => 1,
        ]);
        DB::table('products')->insert([
            'id' => 2,
            'name' => 'Cocosete',
            'reference' => 'Cocossete-202',
            'price' => 1000,
            'cost' => 1200,
            'unit' => 50,
            'status' => 1,
        ]);
        DB::table('products')->insert([
            'id' => 3,
            'name' => 'Pirulin',
            'reference' => 'Pirulin-02',
            'price' => 1000,
            'cost' => 1200,
            'unit' => 50,
            'status' => 1,
        ]);
        DB::table('products')->insert([
            'id' => 4,
            'name' => 'Pan',
            'reference' => 'pan-02',
            'price' => 1000,
            'cost' => 1200,
            'unit' => 50,
            'status' => 1,
        ]);
        DB::table('products')->insert([
            'id' => 5,
            'name' => 'Sussy',
            'reference' => 'Sussy-02',
            'price' => 1000,
            'cost' => 1200,
            'unit' => 50,
            'status' => 1,
        ]);
        DB::table('products')->insert([
            'id' => 6,
            'name' => 'Oreo',
            'reference' => 'Oreo-02',
            'price' => 1000,
            'cost' => 1200,
            'unit' => 50,
            'status' => 1,
        ]);
    }
}
